What is buildbox-common?
========================

buildbox-common is a library containing code used by multiple parts of
BuildBox. Its API is unstable and it should not be used by applications
other than BuildBox.

Currently, buildbox-common contains all the Protocol Buffer definitions
used by BuildBox, as well as code to connect to and interact with
Content-Addressable Storage servers.
